import spritesmith from 'gulp.spritesmith';


export const spritePng = () => {
    return app.gulp.src(`${app.path.src.iconsPng}`, {})
        .pipe(spritesmith({
            imgName: 'menu_sprite.png',
            cssName: 'menu_sprite.css',
        }))
        .pipe(app.gulp.dest('./src/img/sprites'));
}
