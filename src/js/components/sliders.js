/*
Документация по работе в шаблоне:
Документация слайдера: https://swiperjs.com/
Сниппет(HTML): swiper
*/

import Swiper, {Autoplay, Navigation, Pagination, Thumbs, EffectFade} from 'swiper';

// Инициализация слайдеров
function initSliders() {
    // Перечень слайдеров
    if (document.querySelector('[data-swiper]')) {
        new Swiper('[data-swiper]', {
            // Подключаем модули слайдера
            // для конкретного случая
            //modules: [Navigation, Pagination],
            /*
            effect: 'fade',
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            */
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            autoHeight: true,
            speed: 800,
            //touchRatio: 0,
            //simulateTouch: false,
            //loop: true,
            //preloadImages: false,
            //lazy: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            // Arrows
            navigation: {
                nextEl: '.swiper__more .swiper__more--next',
                prevEl: '.swiper__more .swiper__more--prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 16,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 24,
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 32,
                },
            },
            on: {}
        });
    }

    // works slider
    if (document.querySelector('[data-works]')) {
        new Swiper('[data-works]', {
            modules: [Navigation],
            observer: true,
            loop: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 20,
            navigation: {
                nextEl: '[data-works-next]',
                prevEl: '[data-works-prev]',
            },
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 20,
                },
            },
        });
    }

    // partners slider
    if (document.querySelector('[data-partners-one]')) {
        new Swiper('[data-partners-one]', {
            modules: [Navigation],
            observer: true,
            loop: true,
            observeParents: true,
            slidesPerView: 2,
            spaceBetween: 10,
            navigation: {
                nextEl: '[data-partners-one-next]',
                prevEl: '[data-partners-one-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 4,
                    spaceBetween: 20,
                },
                1024: {
                    slidesPerView: 5,
                    spaceBetween: 20,
                },
                1330: {
                    slidesPerView: 5,
                    spaceBetween: 20,
                },
                1500: {
                    slidesPerView: 7,
                    spaceBetween: 20,
                },
            },
        });
    }

    if (document.querySelector('[data-partners-two]')) {
        new Swiper('[data-partners-two]', {
            modules: [Navigation],
            observer: true,
            loop: true,
            observeParents: true,
            slidesPerView: 2,
            spaceBetween: 10,
            navigation: {
                nextEl: '[data-partners-two-next]',
                prevEl: '[data-partners-two-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 4,
                    spaceBetween: 5,
                },
                1024: {
                    slidesPerView: 5,
                    spaceBetween: 5,
                },
                1330: {
                    slidesPerView: 6,
                    spaceBetween: 5,
                },
                1500: {
                    slidesPerView: 7,
                    spaceBetween: 5,
                },
            },
        });
    }

    if (document.querySelector('[data-comments]')) {
        new Swiper('[data-comments]', {
            modules: [Navigation],
            observer: true,
            loop: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 20,
            navigation: {
                nextEl: '[data-comments-next]',
                prevEl: '[data-comments-prev]',
            },
        });
    }

    if (document.querySelector('[data-portfolio]')) {
        new Swiper('[data-portfolio]', {
            modules: [Navigation],
            observer: true,
            loop: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 10,
            navigation: {
                nextEl: '[data-portfolio-next]',
                prevEl: '[data-portfolio-prev]',
            },
        });
    }

    if (document.querySelector('[data-history]')) {
        new Swiper('[data-history]', {
            modules: [Navigation],
            observer: true,
            loop: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 10,
            navigation: {
                nextEl: '[data-history-next]',
                prevEl: '[data-history-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 15,
                },
                1500: {
                    slidesPerView: 3,
                    spaceBetween: 15,
                },
            },
        });
    }


    if (document.querySelector('[data-promo]')) {
        new Swiper('[data-promo]', {
            modules: [Autoplay],
            observer: true,
            loop: true,
            observeParents: true,
            speed: 0,
            slidesPerView: 1,
            spaceBetween: 10,
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
        });
    }
}

window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();
});



