"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import Dropdown from "./components/dropdown.js";
import {WebpMachine} from "webp-hero"
import * as bootstrap from 'bootstrap';

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')

// Сворачиваемые блоки
collapse();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// Меню для мобильной версии
mobileNav();

// Dropdown
Dropdown();


// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false
});

// Menu
const childNav = document.querySelector('[data-nav-child]')

childNav.addEventListener('mouseenter', (event) => {
    const sideNav = event.target.closest('[data-nav-child]').querySelector('.sidenav__menu--child')
    sideNav.style.maxHeight = sideNav.scrollHeight + 'px';
});

childNav.addEventListener('mouseleave', (event) => {
    const sideNav = event.target.closest('[data-nav-child]').querySelector('.sidenav__menu--child')
    sideNav.style.maxHeight = null;
});

// Sidebar
document.addEventListener('click',  (event) => {
    if(event.target.closest('[data-sidebar-collapse]')) {
        document.querySelector('body').classList.toggle('sidebar-close')
        return false
    }
})

// Stat

// ----------------------------
// функция определяет нахождение элемента в области видимости
// если элемент видно - возвращает true
// если элемент невидно - возвращает false
function isOnVisibleSpace(element) {
    var bodyHeight = window.innerHeight;
    var elemRect = element.getBoundingClientRect();
    var offset   = elemRect.top;// - bodyRect.top;
    if(offset<0) return false;
    if(offset>bodyHeight) return false;
    return true;
}

// глобальный объект с элементами, для которых отслеживаем их положение в зоне видимости
let listenedElements = [];
// обработчик события прокрутки экрана. Проверяет все элементы добавленные в listenedElements
// на предмет попадания(выпадения) в зону видимости
window.onscroll = function() {
    listenedElements.forEach(item=>{
        // проверяем находится ли элемент в зоне видимости
        let result = isOnVisibleSpace(item.el);

        // если элемент находился в зоне видимости и вышел из нее
        // вызываем обработчик выпадения из зоны видимости
        if(item.el.isOnVisibleSpace && !result){
            item.el.isOnVisibleSpace = false;
            item.outVisibleSpace(item.el);
            return;
        }
        // если элемент находился вне зоны видимости и вошел в нее
        // вызываем обработчик попадания в зону видимости
        if(!item.el.isOnVisibleSpace && result){
            item.el.isOnVisibleSpace = true;
            item.inVisibleSpace(item.el);
            return;
        }
    });
}

// функция устанавливает обработчики событий
// появления элемента в зоне видимости и
// выхода из нее

function onVisibleSpaceListener(elementId, cbIn, cbOut) {
    // получаем ссылку на объект элемента
    let el = elementId;
    // добавляем элемент и обработчики событий
    // в массив отслеживаемых элементов
    listenedElements.push({
        el: el,
        inVisibleSpace: cbIn,
        outVisibleSpace: cbOut
    });
}

// Counters
let Counters = () => {
    const counters = document.querySelectorAll('[data-counter]')
    if (counters.length > 0) {
        counters.forEach(elem => {
            let counterNum =  parseInt(elem.dataset.counter)
            let counterStep =  parseInt(elem.dataset.step)
            let counterTime =  parseInt(elem.dataset.time)

            let n = 0;
            let t = Math.round(counterTime / (counterNum / counterStep));


            let interval = setInterval(() => {
                n = n + counterStep;
                if (n === counterNum) {
                    clearInterval(interval);
                }
                elem.innerHTML = usefulFunctions.getDigFormat(n);

            }, t);
        });
    }
}

document.addEventListener('scroll', (event) => {
    const body = document.querySelector('body')
    const sticky = document.querySelector('[data-sticky-label]')

    if (sticky.getBoundingClientRect().top < 1) {
        body.classList.add("sticky-fixed");
    } else {
        body.classList.remove("sticky-fixed");
    }
})

// Sliders
import "./components/sliders.js";


window.addEventListener("load", function (e) {

    if(document.querySelector('[data-counters]')) {
        Counters()
    }
});
